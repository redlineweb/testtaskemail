<?php

class MailParser
{
    
    //function for parseOneString
    //return php-array
    public static function parseString($str)
    {
        $re = '/(\w+ \d+ (?:\d\d:){2}\d\d).*\sto=<([^>]*)>.*\sctladdr=<([^>]*)>.*\spri=(\d+).*\sstat=(.*)/';
        
        preg_match_all($re,$str, $info, PREG_SET_ORDER, 0);
        $date = $info[0][1];
        $email = $info[0][2];
        $ctladdr = $info[0][3];
        $pri = $info[0][4];
        $error = $info[0][5];
        return [
            'date' => $date,
            'email' => $email,
            'ctladdr' => $ctladdr,
            'pri' => $pri,
            'error' => $error
            ];
    }
    
    //main function
    public static function Run($logs)
    {
        $values = [];
        foreach ($logs as $log)
        {
            $values[] = self::parseString($log);
        }
        return $values;
    }
    
    //test function
    public static function RunTest()
    {
        $logs = [
            'Feb 10 19:02:17 mail sm-mta[31511]: v1AI2GTr031509: to=<Dalmer.Dalmer@mail.tester-club.com>, ctladdr=<www-data@mail.tester-club.com> (33/33), delay=00:00:01, xdelay=00:00:00, mailer=relay, pri=169116, relay=mxlb.ispgateway.de. [80.67.18.126], dsn=5.0.0, stat=Service unavailable',
            'Feb 25 23:02:17 mail sm-mta[31511]: v1AI2GTr031509: to=<test.sasha@test.ru>, ctladdr=<www-data@mail.tester-club.com> (33/33), delay=00:00:01, xdelay=00:00:00, mailer=relay, pri=169116, relay=mxlb.ispgateway.de. [80.67.18.126], dsn=5.0.0, stat=Bad network',
            'Feb 10 19:02:17 mail sm-mta[31511]: v1AI2GTr031509: to=<test.test.alex@test.ru>, ctladdr=<www-data@mail.tester-club.com> (33/33), delay=00:00:01, xdelay=00:00:00, mailer=relay, pri=169116, relay=mxlb.ispgateway.de. [80.67.18.126], dsn=5.0.0, stat=Service unavailable'
            ];
        
        return self::Run($logs);
    }
}

?>