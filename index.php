<html>
<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
</head>
<body>
<div class="container">
<h1>Sample email log parser</h1>
<form action='/index.php' method=POST>
    <div class="form-group">
    <textarea name="logs" class="form-control" rows=10 >Feb 10 19:02:17 mail sm-mta[31511]: v1AI2GTr031509: to=<Dalmer.Dalmer@mail.tester-club.com>, ctladdr=<www-data@mail.tester-club.com> (33/33), delay=00:00:01, xdelay=00:00:00, mailer=relay, pri=169116, relay=mxlb.ispgateway.de. [80.67.18.126], dsn=5.0.0, stat=Service unavailable
Feb 25 23:02:17 mail sm-mta[31511]: v1AI2GTr031509: to=<test.sasha@test.ru>, ctladdr=<2222www-data@mai2222ter-club.com> (33/33), delay=00:00:01, xdelay=00:00:00, mailer=relay, pri=169333116, relay=mxlb.ispgateway.de. [80.67.18.126], dsn=5.0.0, stat=Bad network
Feb 10 19:02:17 mail sm-mta[31511]: v1AI2GTr031509: to=<test.test.alex@test.ru>, ctladdr=<www-data@ma22233ter-club.com> (33/33), delay=00:00:01, xdelay=00:00:00, mailer=relay, pri=164559116, relay=mxlb.ispgateway.de. [80.67.18.126], dsn=5.0.0, stat=Service unavailable</textarea>
    <p class="help-block">!Important. Each log should be start from new line</p>
    </div>
    <div class="form-group">
    <input type="submit" class="btn btn-success" value='Parse email log'>
    </div>
</form>

<?php
include 'MailParser.php';
if (isset($_POST['logs']))
{
    $logs = $_POST['logs'];
    $logsAr = explode("\n", str_replace("\r", "", $logs));
    if (count($logsAr) > 20)
    {
        ?>
        <div class="alert alert-danger" role="alert">Sorry! But this is demo of the <strong>test task for Upwork</strong>. You can't parse more thant 20 items.</div>
        <?php
    }
    else {
        $values = MailParser::Run($logsAr);
        ?>
        <h4>Default sort by errors</h4>
        <table id="dataTable" class="table table-striped table-bordered">
            <thead>
                <th>Date</th>
                <th>Email</th>
                <th>Error</th>
                <th>Other info</th>
            </thead>
            <tbody>
                
                <?php
                foreach ($values as $value) {
                    ?>
                    <tr>
                        <td><?=$value['date'];?></td>
                        <td><?=$value['email'];?></td>
                        <td><?=$value['error'];?></td>
                        <td><?='ctrlAdr='.$value['ctladdr']." PriCode=".$value['pri'];?></td>
                    </tr>
                    <?
                }
                ?>
            </tbody>
        </table>
        <?
    }
    
}


?>
</div>
<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$('#dataTable').DataTable({
        "order": [[ 3, "desc" ]]
    } );
</script>
</body>
</html>